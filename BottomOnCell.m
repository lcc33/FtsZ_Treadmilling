function singlePF = BottomOnCell(singlePF,kbon,kcapon,tstep,concMonomer,concCap,color,FRET)
GTP = GTPColorAssign(color,FRET);

if  length(singlePF) > 2 && singlePF(end) ~= 5% Must be a pf and not have a cap on bottom
    if rand < kbon.*tstep.*concMonomer  % Event: adding a FtsZ-GTP to bottom
        singlePF = [singlePF; GTP];
    elseif rand < kcapon.*tstep.*concCap % Event: adding a bottom capper to bottom
        singlePF = [singlePF; 5];
    end
else 
    singlePF = 0; % Get rid of pf if it is a dimer
end
end