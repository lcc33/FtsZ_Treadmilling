function [singlePF,singlePFLocation] = TopOffCell(singlePF,ktoff,ktofflow,tstep,singlePFLocation)
if length(singlePF) > 2 % Only do stuff if a nucleated pf
    if (singlePF(2) == 1) || (singlePF(2) == 3)
        if rand < ktoff.*tstep % If second subunit is GDP bound, the top falls off at this rate
            singlePF = singlePF(2:end); % Get rid of top subunit
            singlePFLocation = singlePFLocation + 1; % PF has moved one down
        end
    elseif (singlePF(2) == 2) || (singlePF(2) == 4)
        if rand < ktofflow.*tstep % If the second subunit is GTP bound, this happens as lower rate
            singlePF = singlePF(2:end);
            singlePFLocation = singlePFLocation + 1;
        end
    end
end
    if length(singlePF) < 3 % Get rid of pfs that are only dimers
       singlePF = 0;
    end
end