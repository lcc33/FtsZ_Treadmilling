clear all; close all;

tstep = 0.01;
time = 30;

% Kinetic Parameters
kbon = 10;
kboff = 1;
ktoff = 8;
ktofflow = 1;
KnD = 500;
indGtpaseRate = 1/2; % Rate that each individual bound FtsZ can hydrolyze per a second

concTotalFtsZ = 3.12;

% Experimental Inputs
color = 1;
FRET = 0;

%% Run Treadmilling Model
[allPFs, distanceMoved,hydPerSecond,concMonomerPerSecond] = TreadmillCells(time,tstep,concTotalFtsZ,color,FRET,kbon,kboff,ktoff,ktofflow,KnD,indGtpaseRate);

%% Analyze Collected Data
figure
len = 2;
wid = 2;

% SS PF Speed
subplot(len,wid,1)
hist(distanceMoved{end})
xlabel('SS Distance Moved (subunits)')
ylabel('Frequency')

% SS PF Length
pfLength = zeros(1,length(allPFs));
for ii = 1:length(allPFs)
    pfLength(ii) = length(allPFs{ii});
end
subplot(len,wid,2)
hist(pfLength)
xlabel('SS PF Length (subunits)')
ylabel('Frequency')

% Hydrolysis
hydPerMinute = num2conc(hydPerSecond)./concTotalFtsZ.*60;
subplot(len,wid,3)
plot(1:time,hydPerMinute)
xlabel('Time (sec)')
ylabel('GTPase')

% Concentration of Monomer
subplot(len,wid,4)
plot(0:time,concMonomerPerSecond)
xlabel('Time (sec)')
ylabel('[Monomer]')