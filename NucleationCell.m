%% Nucleation is very rare at steady state, but is not rare at the beginning of the reaction. 

function [allPFs,dimer] = NucleationCell(allPFs, tstep,kbon, color,FRET,concMonomer,KnD,dimer)
GTP = GTPColorAssign(color,FRET); % Tells us what represents GTP in our experiment

% Calculate reverse and forward nucleation reactions based off Kd of
% nuclation (KnD)
knr = 10;  % Assume 10 for the reverse reaction rate.
knf = 1./KnD;
%% Calculate Rate of Production of Dimers
% At the very beginning, our dimers are not yet at steady state. Do this
% calculation to determine rate of dimers created
rate = knf.*concMonomer.^2.*tstep; % Forward rate of creating dimers
rate = rate - knr.*rate.*tstep; % Total rate of creating dimers is forward minus reverse
rate = conc2num(rate); % Convert this rate to a number of dimers created per a time step

% Stochastically round rate to the nearest whole number. Example - if rate
% is 0.2, you would most likely not have a new dimer; however, 20% of the
% time you would want a new dimer, so you can't just round the 0.2 down to
% 0. We want to include the small chance a dimer will form about every five
% time steps. 
if rate > 1
    rate = round(rate);
elseif rate < 0
    rate = 0;
else
    r = rand;
    if r < rate
        rate = 1;
    else
        rate = 0;
    end
end

dimer = dimer + rate; % Increase the number of dimers by the rate that monomer changed to dimers

nucleationRate = kbon.*concMonomer.*tstep; % Each dimer can elongate to become a trimer (pf) with the same rate that a pf elongates
r = rand(1,dimer); % Generate random numbers for each dimer. 

ind = find(r < nucleationRate); % These random numbers dictate if each dimer will become a trimer. Trimers are considered PFs
addedNumPFs = length(ind);

if addedNumPFs > 0 % Add a trimer of FtsZ-GTP to the PFs
    addedPFs = [GTP; GTP; GTP;];
    for ii = 1:addedNumPFs
        len = length(allPFs);
        allPFs{len+1} = addedPFs;
    end
end

dimer = dimer - addedNumPFs; % Dimers converted to PFs are deleted from count

end