function ssLength(proto)
sslen = zeros(1,size(proto,2));
for qq = 1:length(sslen)
    m = find(proto(:,qq));
    sslen(qq) = length(m);
end
histogram(sslen)
m = mean(sslen);
s = std(sslen);
txt = ['Length of Protofilament after Time; Avg. =  ',num2str(m),' StDev = ',num2str(s)];
xlabel(txt)
ylabel('Frequency')
end