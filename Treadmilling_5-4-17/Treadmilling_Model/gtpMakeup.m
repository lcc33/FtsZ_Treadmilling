function gtpMakeup(proto)
gtp = zeros(1,size(proto,2));
for qq = 1:size(proto,2)
    m = find(proto(:,qq)==2);
    gtp(qq) = length(m);
end

histogram(gtp)
m = mean(gtp);
s = std(gtp);
txt = ['Number of FtsZ-GTP in PF after Time; Avg =  ',num2str(m),' StDev = ',num2str(s)];
xlabel(txt)
ylabel('Frequency')
end