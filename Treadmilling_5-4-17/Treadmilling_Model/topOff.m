function y = topOff(singleproto,ktr,kbr, tstep)
y = singleproto;
topoff = ktr.*tstep;
bottomoff = kbr.*tstep;
r = rand;
q = find(y);  % indexing location of subunits in the pf
if any(y) == 1 && length(q)>1 % must be stuff in pf and more than one monomer

    if  y(q(2)) == 1 && r <= topoff % penultimate must be GDP bound, the probability of falling off is higher
        y(q(1)) = 0; % taking off the last subunit off the bottom      
    elseif  y(q(2)) == 2 && r <= bottomoff % penultimate must be GTP bound, the probability of falling off is lower
        y(q(1)) = 0; % taking off the last subunit off the bottom      
    end
end

if length(find(y)) == 1  %If protofilament is just a single subunit, it goes back into pool           
    y = zeros(length(y),1);
end

end