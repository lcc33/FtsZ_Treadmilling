%% Simulation of pfs falling off and on and hydrolysis

% Rate Constants
kbf = 9;
kbr = 2;
ktr = 7;
gtpaserate = 0.03;

% Initial Protofilaments and FtsZ
totalftsz = 6750; %total ftsZ subunits in cell
startlength = 50; % Each pf is 50 long at start
cap = 3; % 3 long cap of FtsZ-GTP
totalp = 100; % the total number of pfs is 100
len = startlength.*4;

% How long to run
tstep = 0.01; %the time step is set to 0.01 seconds
time = 20; % this simulates 20 seconds

% Initiate Values
totalstep = time./tstep; %total number of time steps
csubunit = zeros(1,totalstep);
allprotos = zeros(len,totalp,totalstep);
proto = initialPFs(startlength,cap,totalp,len);
speed = zeros(totalp,totalstep);

disttravel = zeros(totalstep,totalp);

% Loop for simulating stuff going off and on for each time frame:
for ii = 1:totalstep % cycling through time
    
    subunits =  totalftsz - length(find(proto>0)); % calculate new number of free subunits
    csubunit(ii) = subunits.*10.^6/(2.*10.^-15.*6.022.*10.^23); % calculate concentration of free subunits
    
    for mm = 1:totalp % cycling through all of the protofilaments and making sure same action is done to each pf

        beginninglocation = Spot(proto(:,mm)); % Determine where pf starts
        
        proto(:,mm) = bottomOff(proto(:,mm),kbr, tstep); %Function for falling off bottom
        proto(:,mm) = bottomOn(proto(:,mm),kbf,csubunit(ii),tstep); %Function for adding to bottom
        proto(:,mm) = topOff(proto(:,mm),ktr,kbr, tstep); %Function for falling off top
        proto(:,mm) = gtpaseFunction(proto(:,mm),gtpaserate,tstep); %Function for gtpase
        
        endlocation = Spot(proto(:,mm)); % Determine where pf ends
        disttravel(ii,mm) = endlocation - beginninglocation; % Find distance traveled by pf over time frame
        
        proto(:,mm) = shiftToTop(proto(:,mm)); %Function to shift pf to top when gets to bottom
    end
 allprotos(:,:,ii) = proto; %stores current pfs and saves them all over time
end
%% Output data: 

% Plots protofilaments over time
figure(1)
multiplier = 30;
tbreak = .1;
plotTreadmilling(allprotos,multiplier,tbreak,tstep,time) %plots every 10.*tstep seconds

% Analysis
figure(2)
subplot(4,1,1)
ssLength(proto) %Plots the ending length of PFs
subplot(4,1,2)
gtpMakeup(proto) %Plots the GTP makeup of ending PFs
subplot(4,1,3)
gdpMakeup(proto) %Plots the GTP makeup of ending PFs
subplot(4,1,4)
freeMonomerOverTime(csubunit,time,tstep) % plots concentration of free monomer over time)

% Plots average distance travelled and standard deviation
figure(3)
speedOutputs(disttravel,time,tstep)
