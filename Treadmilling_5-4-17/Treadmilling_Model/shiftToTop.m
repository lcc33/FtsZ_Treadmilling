function y = shiftToTop(singleproto)
y = singleproto;
if y(end-3) > 0
    q = find(y,1);
    y = circshift(y,length(y)-q+1); %shifts the pf to top
end
end
