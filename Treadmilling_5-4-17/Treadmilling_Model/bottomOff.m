function y = bottomOff(singleproto,kbr,tstep)
y = singleproto;
r = rand;
bottomoff = kbr.*tstep;

if any(singleproto) == 1 & r <= bottomoff
   q = find(singleproto>0); % Finding location of subunits in the protofilament matrix
   y(q(end)) = 0; %Taking off one subunit from end of protofilament
end

if length(find(y)) == 1;  %If protofilament is just a single subunit, it goes back into pool           
    y = zeros(length(y),1);
end

end