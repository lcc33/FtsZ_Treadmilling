function y = initialPFs(startlength,cap,totalp,len)
y = zeros(len,totalp);
mid = fix(len/2);
y(mid-startlength:mid-cap,1:end) = 1; %the protofilaments have a three GTP bound cap on top
y(mid-cap+1:mid,1:end) = 2; %the pfs are 50 subunits long. The 1 represents GDP bound
end