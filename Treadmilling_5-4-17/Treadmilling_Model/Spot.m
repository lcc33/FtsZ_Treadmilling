function middlepoint = Spot(singlepf)
pf = find(singlepf);
halflength = fix(length(pf)/2);
middlepoint = pf(halflength);
end