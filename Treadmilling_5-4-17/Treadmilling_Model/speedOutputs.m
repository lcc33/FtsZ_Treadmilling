function speedOutputs(disttravel,time,tstep)
distravel = disttravel.*5; %each monomer is 5 nm long.
t = 0:tstep:time;
subplot(2,1,1)
plot(t(2:end),mean(disttravel,2))
ylabel('Average Distance Traveled (nm)')
subplot(2,1,2)
plot(t(2:end),std(disttravel,0,2));
ylabel('Std Dev')
xlabel('Time (s)')
end