function gdpMakeup(proto)
gdp = zeros(1,size(proto,2));
for qq = 1:size(proto,2)
    m = find(proto(:,qq)==1);
    gdp(qq) = length(m);
end

histogram(gdp)
m = mean(gdp);
s = std(gdp);
txt = ['Number of FtsZ-GDP in PF after Time; Avg =  ',num2str(m),' StDev = ',num2str(s)];
xlabel(txt)
ylabel('Frequency')
end