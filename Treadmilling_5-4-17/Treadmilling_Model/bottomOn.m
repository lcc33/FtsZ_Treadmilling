function y = bottomOn(singleproto,kbf,csubunit,tstep)
y = singleproto;
r = rand;
bottomon = kbf.*csubunit.*tstep;
if any(y) == 1 && r <= bottomon
    q = find(y>0); % finding location of subunits in the pf matrix
    q = q(end)+1; %indexing the location of one above the first subunit in the pf
    y(q) = 2; % Adding one GTP bound FtsZ to the top
end
end