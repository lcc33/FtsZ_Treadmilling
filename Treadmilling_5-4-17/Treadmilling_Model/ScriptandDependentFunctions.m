function ScriptandDependentFunctions(scriptToCheck)

% Create a new folder to put everything in
today = datetime;
today = string(today);

newFolder = strcat(scriptToCheck,today);
newFolder = char(newFolder);
mkdir(newFolder)

% Create a list of all the files used
files = matlab.codetools.requiredFilesAndProducts(scriptToCheck);

% Move files to new folder
for ii = 1:length(files)
    y = char(files{ii});
    movefile(y,newFolder); 
end

end