function y = gtpaseFunction(singleproto,gtpase,tstep)
y = singleproto;

if any(y) == 1
    r = rand(length(y),1); %make a new random number matrix that is same length as pf
    gtpaserate = gtpase.*tstep;
    
    for jj = 1:length(y) % cycle through the length of the pf
        if y(jj) ==2 && r(jj) <= gtpaserate  % if the random number is less than the rate calculated and it is GTPbound...
            y(jj) = 1; % it now becomes GDP bound
        end
    end
end
end
