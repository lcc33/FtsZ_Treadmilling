function plotTreadmilling(allprotos,multiplier,tbreak,tstep,time)
o = size(allprotos,3);
pull = 1:10:o;
suballprotos = allprotos(:,:,pull);
timesampled = 0:tstep:time;
timeplotted = timesampled(pull);

for ii = 1:length(pull)
    subplot(5,3,1:9)
    image(suballprotos(:,:,ii).*multiplier)
    xlabel('Protofilament')
    
    subplot(5,3,13:15)
    barh(timeplotted(ii))
    xlim([0,time])
    xlabel('Time (s)')
    pause(tbreak)
end