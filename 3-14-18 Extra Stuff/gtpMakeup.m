function gtpMakeup(proto, color)
[~, GTP] = colorAssigner(color);

gtp = zeros(1,lastProtofilament(proto));
for qq = 1:lastProtofilament(proto)
    m = find(proto(:,qq)==GTP);
    gtp(qq) = length(m);
end
% edges = 0:5:60;
% histogram(gtp,edges)
ind = find(gtp);
gtp = gtp(ind);
histogram(gtp)
m = mean(gtp);
s = std(gtp);
txt = ['Number of FtsZ-GTP in PF after Time; Avg =  ',num2str(m),' StDev = ',num2str(s)];
xlabel(txt)
ylabel('Frequency')
end