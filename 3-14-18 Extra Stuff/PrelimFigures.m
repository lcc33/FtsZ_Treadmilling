clear all; close all;

MciZ = 0; % Boolean, is MciZ in system?
color = 1; % See protocolormap file for key
FRET = 0; % Boolean, is FRET occuring?
initialpfs = 0; % Are there any initial pfs? If yes, make equal to matrix of pfs.
tstep = 0.01;
wildtype = 50;
gtpase = wildtype;

%% Length and screen shot of pfs with 3 uM FtsZ after 30 seconds
time = 30;
concFtsZ = 3;

[protos_sec] = FtsZ_Treadmilling(color,time,MciZ,FRET,initialpfs,concFtsZ,gtpase);

figure(2)
finalprotos = protos_sec(:,:,end); %the last protofilaments
row = 3;
col = 2;
%subplot(row,col,4)
[lenavg, lenstd] = ssLength(finalprotos)
text(0.02,0.98,'D','Units', 'Normalized', 'VerticalAlignment', 'Top')

% subplot(row,col,1)
% image(finalprotos(500:900,1:80));
% colormap(protocolormap)
% xlabel('PF Number')
% t = text(0.02,0.98,'A','Units', 'Normalized', 'VerticalAlignment', 'Top');
% t.Color = 'w';

%% Mix pfs with GTPase
%subplot(row,col,5)
figure(2)
secondruntime = 30;
wthalftime = FRETexperiment(gtpase,secondruntime)
legend('100% GTPase')
text(0.02,0.98,'E','Units', 'Normalized', 'VerticalAlignment', 'Top')

% subplot(row,col,6)
gtpase = 0;
secondruntime = 200;
muthalftime = FRETexperiment(gtpase,secondruntime)
legend('0% GTPase')
text(0.02,0.98,'F','Units', 'Normalized', 'VerticalAlignment', 'Top')

%% Time to steady state and GTPase
%subplot(row,col,2)
figure(3)
time = 20;
gtpase = wildtype;
concFtsZ = 3.12;
[~,csubunit,~,hydsec] = FtsZ_Treadmilling(color,time,MciZ,FRET,initialpfs,concFtsZ,gtpase);
%yyaxis left
freeMonomerOverTime(csubunit,time,tstep) % plots concentration of free monomer over time)
Cc = mean(csubunit(end-5:end))
xlabel('Time (s)')
text(0.02,0.98,'B','Units', 'Normalized', 'VerticalAlignment', 'Top')
hold on

t = 0:tstep:time;
yyaxis right
plot(0:time,hydsec)
xlabel('Time (s)')
ylabel({'GTPase','(GTP/min/FtsZ)'})

ssGTPase = mean(hydsec(end-3:end))

%% Histogram of final speed
%subplot(row,col,3)
figure(4)
concFtsZ = 7;
concFtsZ = concFtsZ+0.46.*concFtsZ;
[~,~,~,~,~,distmoved,~] = FtsZ_Treadmilling(color,time,MciZ,FRET,initialpfs,concFtsZ,gtpase);

edges = 0:5:70;
ticks = 0:10:70;
hist(distmoved,edges);
ylabel('Frequency');
xlabel('Speed (nm/s)');
xticks(ticks);
speedavg = mean(distmoved)
speedstdev = std(distmoved,0,2)
text(0.02,0.98,'C','Units', 'Normalized', 'VerticalAlignment', 'Top')

%% Treadmilling Diagrams
highdiagram = imread('Diagram_HighGTPase.tiff');
highdiagram = sum(highdiagram,3);

lowdiagram = imread('Diagram_LowGTPase.tiff');
lowdiagram = sum(lowdiagram,3);

subplot(1,2,1)
imagesc(highdiagram)
colormap gray
subplot(1,2,2)
imagesc(lowdiagram)
colormap gray