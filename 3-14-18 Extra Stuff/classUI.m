classdef classUI < handle

    properties
        Figure
        TitlePanel
        Title
        Panel1
        Panel2
        Panel3
        Panel4
        Panel5
        Panel6
        
        TreadmillDiagram
        TreadmillLabel
        TreadmillDiagramLow
        CapDiagram
        NucleationDiagram
        GTPaseDiagram
        GTPaseInput
        GTPaseInputPanel
        TimeInput
        TimeLabel
        ConcFtsZInput
        ConcFtsZLabel
        KbOnInput
        KbOnInputPanel
        KbOnInputLabel
        KbOffInput
        KbOffInputPanel
        KbOffInputLabel
        KtOffInput
        KtOffInputPanel
        KtOffInputLabel
        KtLowOffInput
        KtLowOffInputPanel
        KtLowOffInputLabel
        KRevFavInput
        BottomCap
        PreformedPFs
        PreformedPFsLabel
        GoButton
        CloseButton
    end
    
    methods
        function app = classUI
            app.Figure = figure('MenuBar','none','NumberTitle','off','Name','Treadmilling Experiment','Position',[200 50 900 700],'CloseRequestFcn',@closeApp);
            app.TitlePanel = uipanel('Parent',app.Figure,'Position',[0.3 0.92 0.4 0.06]); 
            app.Title = uicontrol('Parent',app.TitlePanel,'Style','Text','String','FtsZ Treadmilling Experiment','fontsize',20,'Position',[25 10 300 24]);
            
            cd ModelDiagrams
            
            app.Panel1 = uipanel('Parent',app.Figure,'BackgroundColor','w','Position', [.025 .05 .3 0.85]);
            app.TreadmillDiagram = axes('Parent',app.Panel1,'Position',[0 0 1 1]); % Creates axes to plot on
            highDiagram = imread('Slide1.jpg');
            app.TreadmillDiagram = imshow(highDiagram);
            app.TreadmillLabel = uicontrol('Parent', app.Panel1,'BackgroundColor','w','Style','Text','String','Top subunit is in low affinity state.','FontSize',14,'Position',[2 450 115 50]);
            
            app.KbOnInputPanel = uipanel('Parent', app.Panel1,'Position',[0.01 0.15 0.3 0.04]);
            app.KbOnInput = uicontrol('Parent',app.KbOnInputPanel,'Style','edit','String','10','Position',[0 0 30 20]);
            app.KbOnInputLabel = uibutton('Parent',app.KbOnInputPanel,'String','\muM^-^1s^-^1','Interpreter','tex','Position',[30 0 50 20]);
            
            app.KbOffInputPanel = uipanel('Parent',app.Panel1,'Position',[0.71 0.15 0.23 0.04]);
            app.KbOffInput = uicontrol('Parent',app.KbOffInputPanel,'Style','edit','String','1','Position',[0 0 30 20]);
            app.KbOffInputLabel = uibutton('Parent',app.KbOffInputPanel,'String','s^-^1','Interpreter','tex','Position',[30 0 30 20]);
            
            app.KtOffInputPanel = uipanel('Parent',app.Panel1,'Position',[0.71 0.82 0.23 0.04]);
            app.KtOffInput = uicontrol('Parent',app.KtOffInputPanel,'Style','edit','String','8','Position',[0 0 30 20]);
            app.KtOffInputLabel = uibutton('Parent',app.KtOffInputPanel,'String','s^-^1','Interpreter','tex','Position',[30 0 30 20]);
            
            app.Panel2 = uipanel('Parent',app.Figure,'BackgroundColor','w','Position', [.35 .41 .3 0.49]);
            app.TreadmillDiagramLow = axes('Parent',app.Panel2,'Position',[0 0 1 1]); % Creates axes to plot on
            LowDiagram = imread('Slide2.jpg');
            [x, ~, ~] = size(LowDiagram);
            LowDiagram = LowDiagram(1:int64(x*.58),:,:);
            app.TreadmillDiagramLow = imshow(LowDiagram);
            
            app.KtLowOffInputPanel = uipanel('Parent',app.Panel2,'Position',[0.71 0.69 0.23 0.07]);
            app.KtLowOffInput = uicontrol('Parent',app.KtLowOffInputPanel,'Style','edit','String','1','Position',[0 0 30 20]);
            app.KtLowOffInputLabel = uibutton('Parent',app.KtLowOffInputPanel,'String','s^-^1','Interpreter','tex','Position',[30 0 30 20]);
            
            app.Panel3 = uipanel('Parent',app.Figure,'BackgroundColor','w','Position',[0.67 .4 .3 0.5]);
            app.CapDiagram = axes('Parent',app.Panel3,'Position',[0 0 1 1]); % Creates axes to plot on
            capdiagram = imread('Slide3.jpg');
            [x, ~, ~] = size(capdiagram);
            capdiagram = capdiagram(int64(x*.4):end,:,:);
            app.CapDiagram = imshow(capdiagram);
            
            cd ../
            
            app.Panel4 = uipanel('Parent',app.Figure,'BackgroundColor','w','Position',[0.35 .22 .3 0.17]);
            app.NucleationDiagram = axes('Parent',app.Panel4,'Position',[0 0 1 1]); % Creates axes to plot on
            nucleation = imread('DimerReaction.jpg');
            app.NucleationDiagram = imshow(nucleation);
            app.KRevFavInput = uicontrol('Parent',app.Panel4,'Style','edit','String','100','Position',[140 25 50 20]);
            
            %app.BottomCap = uicontrol('Style','checkbox','Position',[
            
            app.Panel5 = uipanel('Parent',app.Figure,'BackgroundColor','w','Position',[0.35 .05 .3 0.15]);
            app.GTPaseDiagram = axes('Parent',app.Panel5,'Position',[0 0 1 1]);
            gtpasediagram = imread('GTPaseDiagram.jpg');
            [x, y, ~] = size(gtpasediagram);
            gtpasediagram = gtpasediagram(int64(x.*0.07):int64(x.*0.28),1:int64(0.8.*y),:);
            app.GTPaseDiagram = imshow(gtpasediagram);
            app.GTPaseInputPanel = uipanel('Parent',app.Panel5,'Position',[0.4 0.2 0.2 0.2]);
            app.GTPaseInput = uicontrol('Parent',app.GTPaseInputPanel,'Style', 'edit','String','50','Position', [0 0 50 20]);
            
            app.Panel6 = uipanel('Parent',app.Figure,'Position',[0.67 .05 .3 0.32]);
            app.PreformedPFs = uicontrol('Parent',app.Panel6,'Style','checkbox','Position', [10 170 20 20]);
            app.PreformedPFsLabel = uicontrol('Parent',app.Panel6,'Style','Text','String','Run Experiment on Preformed PFs','FontSize',14,'Position',[30 172 225 20]);
            app.TimeInput = uicontrol('Parent',app.Panel6,'Style','edit','String','10','Position', [90 130 50 20]);
            app.TimeLabel = uicontrol('Parent',app.Panel6,'Style','Text','String','Time (s)','FontSize',14,'Position',[10 130 80 20]);
            app.ConcFtsZInput = uicontrol('Parent',app.Panel6,'Style','edit','String','6','Position', [90 100 50 20]);
            app.ConcFtsZLabel = uicontrol('Parent',app.Panel6,'Style','Text','String','[FtsZ] (uM)','FontSize',14,'Position',[10 100 80 20]);
           
            app.GoButton = uicontrol('Parent',app.Panel6,'Style','pushbutton','String','Run Experiment','FontSize',16,'Position',[10 50 120 20],'BackgroundColor',[152,251,152]/256,'Callback',@startExperiment);
            app.CloseButton = uicontrol('Parent',app.Panel6,'Style','pushbutton','String','Quit Program','FontSize',16,'Position', [10 10 120 20],'BackgroundColor',[256,92,92]/256,'Callback',@closeExperiment);
            
            function closeExperiment(source,event)
                delete(app.Figure)
            end
           
            function startExperiment(source,event)
                
                gtpase = str2double(app.GTPaseInput.String);
                kbon = str2double(app.KbOnInput.String);
                kboff = str2double(app.KbOffInput.String);
                ktoff = str2double(app.KtOffInput.String);
                kRevFav = str2double(app.KRevFavInput.String);
                ktlowoff = str2double(app.KtLowOffInput.String);
                concFtsZ = str2double(app.ConcFtsZInput.String);
                time = str2double(app.TimeInput.String);
                
                color = 1;
                FRET = 0;
                initialpfs = 0;
                tstep = 0.01;
                bottomCap = 0;
                
                closereq
                
                [protoSec,concMonomer,hydSec,distMoved] = FtsZ_Treadmilling(concFtsZ,bottomCap,gtpase,time,color,FRET,initialpfs,tstep,kbon,kboff,ktoff,ktlowoff,kRevFav);
                
                finalprotos = protoSec(:,:,end);
                
                %% Output Data
                % Shows snapshots of PFs over time
                figure
                len = 3;
                wid = 2;
                subplot(len,wid,1)
                image(protoSec(:,:,2))
                colormap(protocolormap)
                title('One Second after Nucleation')
                subplot(len,wid,2)
                image(finalprotos);
                colormap(protocolormap)
                title('Final Protofilaments')
               
                % Plot Average PF Speed
                subplot(len,wid,3)
                edges = 0:5:70;
                ticks = 0:10:70;
                m = histogram(distMoved,edges);
                set(m, 'FaceColor',[.05 .05 .05])
                ylabel('Frequency');
                xlabel('Speed (nm/s)');
                xticks(ticks);
                speedavg = mean(distMoved)
                speedstdev = std(distMoved,0,2)
                title('Steady State PF Speed')
                
                subplot(len,wid,4)
                [lenavg, lenstd] = ssLength(finalprotos) % Plots pf legnth
                title('Steady State PF Length')
                
                subplot(len,wid,5)
                yyaxis left
                freeMonomerOverTime(concMonomer,time,tstep) % plots concentration of free monomer over time)
                Cc = mean(concMonomer(end-5:end))
                xlabel('Time (s)')

                t = 0:tstep:time;
                yyaxis right
                plot(0:time,hydSec)
                xlabel('Time (s)')
                ylabel({'GTPase','(GTP/min/FtsZ)'})
                title('Protofilament Kinetics')
                ssGTPase = mean(hydSec(end-3:end)) 
            end
           
        end
    end
end