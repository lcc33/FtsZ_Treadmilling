color = 1;
time = 30;
tstep = 0.01;
MciZ = 0;
FRET = 0;
initialpfs = 0;

runs = 10;

totalerror = zeros(1,runs);
gtpase = zeros(1,runs);
den = zeros(1,runs);
knr = 10;
concFtsZ = 3;

for ii = 1:runs
    
    gtpase(ii) = randi([20,60],1);
    den(ii) = randi([100,1000],1);
    knf = 1./den;
    
    [protos_sec,csubunit,pflen,hydsec,checkprobs,distmoved] = FtsZ_Treadmilling(color,time,MciZ,FRET,initialpfs,concFtsZ,gtpase(ii),knf(ii),knr);
    
    finalspeed = mean(distmoved);
    finalpflen = mean(pflen);
    hydsec = hydsec(end-5:end); %GTPase rate each second
    ssGTPase = mean(hydsec(end-5:end));
    Cc = mean(csubunit(end-5:end));
    sstime = timeToSS(csubunit,time,tstep);
    
    totalerror(ii) = abs(0.7 - Cc)./0.7 + abs(200 - finalpflen)./200 + abs(ssGTPase - 5)./5 + abs(finalspeed - 29)./29; 
end

endsound


%gtpase = [18,31,54,45,16,55,37,37,54,35,50,21,48,30]

%den = [10816,16997,5034,17065,17420,16045,10997,16133,3552,19208,10618,8394,3433,15925]