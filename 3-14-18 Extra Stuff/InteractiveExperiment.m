clear all; close all

figure(1)
row = 1;
col = 2;
subplot(row,col,1)
inputPrompt = {'Concentration of FtsZ (uM):','Time (sec):'};
inputTitle = 'Initiate Treadmilling Experiment';
lines = 1;
inputDefault = {'6','10'};
input = inputdlg(inputPrompt,inputTitle,lines,inputDefault);
inputs = cell2vector(input);


concFtsZ = inputs(1);
time = inputs(2);

color = 1;
FRET = 0;
initialpfs = 0;
tstep = 0.01;
bottomCap = 0;
gtpase = 50;
kbon = 10;
kboff = 1;
ktoff = 8;
ktlowoff = 1;
kRevFav = 100;

if FRET == 1
    [initialpfs,protoSec,concMonomer,hydSec,distMoved,mixHalfTime] = FRETexperiment(concFtsZ,gtpase,time,tstep,kbon,kboff,ktoff,ktlowoff,kRevFav);
else 
    [protoSec,concMonomer,hydSec,distMoved] = FtsZ_Treadmilling(concFtsZ,bottomCap,gtpase,time,color,FRET,initialpfs,tstep,kbon,kboff,ktoff,ktlowoff,kRevFav);
    
    finalpfs = protoSec(:,:,end);
    figure(2)
    row = 2;
    col = 2;

    % Plot Last PFs
    subplot(row,col,1)
    imagesc(finalpfs)

    % Plot GTPase and [Free FtsZ] Over Time
    subplot(row,col,2)
    yyaxis left
    plot(0:time,hydSec)
    ylabel({'GTPase','(GTP/min/FtsZ)'})
    hold on

    yyaxis right
    plot(tstep:tstep:time,concMonomer)
    ylabel('[Free FtsZ] (uM)')


    % Plot PF length at End
    subplot(row,col,3)
    y = lastProtofilament(finalpfs);
    sslen = zeros(1,y); % Initiate vector that holds the length of each pf
    for qq = 1:length(sslen) % Loop that determines length (in subunits) of each pf
        m = find(finalpfs(:,qq));
        sslen(qq) = length(m);
    end
    sslen = sslen.*4.3; % Subunits are all 4.3 nm tall, so multiply by this to get actual length

    ind = find(sslen); % Finds where greater than zero legnth of pf
    sslen = sslen(ind); % Only uses nonzero pfs
    edges = 0:10:500; % How to bin the histogram
    histogram(sslen,edges) % Plot a histogram

    % Plot Final Speed
    subplot(row,col,4)
    edges2 = 0:5:70;
    histogram(distMoved,edges2); % Histogram of speed of pfs at end
    end

