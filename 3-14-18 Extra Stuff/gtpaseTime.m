function hydsec = gtpaseTime(totalhyd,time,tstep,concFtsZ,csubunit)

t = 0:tstep:time;
ind = find(mod(t,1) == 0);
hydsec = zeros(1,length(ind)-1);
Cc = mean(csubunit(end-5:end));
for ii = 2:length(ind)
    hydsec(ii) = sum(totalhyd(ind(ii-1):ind(ii)));
end
hydsec = num2conc(hydsec).*60./concFtsZ;
end