function plotTreadmilling(protos_sec,tbreak,time)
for rr = 1:time
    subplot(4,4,1:12)
     image(protos_sec(:,:,rr))
     colormap(protocolormap)
     pause(tbreak);
     
     subplot(4,4,13:16)
     barh(rr)
     xlim([0 time])
     xlabel('Time (s)')
end
end