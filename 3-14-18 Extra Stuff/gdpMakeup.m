function gdpMakeup(proto,color)
[GDP, ~] = colorAssigner(color);

gdp = zeros(1,lastProtofilament(proto));
for qq = 1:lastProtofilament(proto)
    m = find(proto(:,qq)==GDP);
    gdp(qq) = length(m);
end
% edges = 0:5:60;
% histogram(gdp,edges)
ind = find(gdp);
gdp = gdp(ind);
histogram(gdp)
m = mean(gdp);
s = std(gdp);
txt = ['Number of FtsZ-GDP in PF after Time; Avg =  ',num2str(m),' StDev = ',num2str(s)];
xlabel(txt)
ylabel('Frequency')
end