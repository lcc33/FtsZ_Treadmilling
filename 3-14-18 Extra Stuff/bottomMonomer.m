function bottom = bottomMonomer(proto)
m = size(proto, 2);
bottom = zeros(1,m);
for ii = 1:m
    if any(proto(:,ii)) ==1 
        ind = find(proto(:,ii));
        bottom(ii) = ind(end);
    else
        bottom(ii) = 1000;
    end
end