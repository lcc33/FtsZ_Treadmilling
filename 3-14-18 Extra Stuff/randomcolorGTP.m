function GTP = randomcolorGTP(FRET)

if FRET == 1 && rand >= 0.5
    GTP = 4;
elseif FRET == 1 && rand < 0.5
    GTP = 2; 
end

end