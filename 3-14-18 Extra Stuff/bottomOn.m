function [y,checkprobs] = bottomOn(singleproto,kbf,csubunit,tstep, color, FRET,probs,checkprobs)

GTP = GTPColorAssign(color,FRET);

y = singleproto;
bottomon = kbf.*csubunit.*tstep;

    if bottomon > 1
        checkprobs = checkprobs+1;
    end

if any(y) == 1 && probs <= bottomon
    q = find(y); % finding location of subunits in the pf matrix
    q= q(end)+1; %indexing the location of one below the bottom subunit in pf
    if y(q-1) == 5
        y(q) = 0;
    else
        y(q) = GTP; % Adding one GTP bound FtsZ to the bottom
    end
end
end