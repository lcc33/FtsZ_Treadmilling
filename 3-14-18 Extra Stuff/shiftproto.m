function proto = shiftproto(proto) % Shifts all pfs to the left

[~,n] = size(proto);
for ii = 1:n
    if any(proto(:,ii)) == 0
        proto(:,ii:end-1) = proto(:,ii+1:end);
        proto(:,end) = 0;
    end
end

end