clear all; close all;

MciZ = 0; % Boolean, is MciZ in system?
color = 2; % See protocolormap file for key
FRET = 0; % Boolean, is FRET occuring?
initialpfs = 0; % Are there any initial pfs? If yes, make equal to matrix of pfs.
tstep = 0.01;

time = 30;
concFtsZ = 3.12;

gtpase = 30;
%den = 1682;
den = 500;
knf = 1./den;
knr = 10;

[protos_sec,csubunit,pflen,hydsec,checkprobs,distmoved,speed] = FtsZ_Treadmilling(color,time,MciZ,FRET,initialpfs,concFtsZ,gtpase,knf,knr);

finalprotos = protos_sec(:,:,end); %the last protofilaments

% Treadmilling Analysis

%Plots protofilaments over time
figure(1)
tbreak = 1;
plotTreadmilling(protos_sec,tbreak,time)

% Analysis
figure(2) % Plots histogram of pfs at end
subplot(3,1,1)
ssLength(finalprotos) %Plots the ending length of PFs
subplot(3,1,2)
gtpMakeup(finalprotos,color) %Plots the GTP makeup of ending PFs
subplot(3,1,3)
gdpMakeup(finalprotos,color) %Plots the GTP makeup of ending PFs

% Plots average distance travelled and standard deviation
figure(3)
row = 4;
col = 1;
subplot(row,col,1)
plot(speed)
m = mean(speed(end-5:end));
txt = ['Steady State =  ',num2str(m)];
ylabel('Speed (nm/s)')
legend(txt)

subplot(row,col,2)
plot(pflen)
ylabel('PF Length (subunits)')
m = mean(pflen(end-5:end));
txt =  ['Steady State = ',num2str(m)];
legend(txt)

subplot(row,col,3)
freeMonomerOverTime(csubunit,time,tstep) % plots concentration of free monomer over time)
Cc = mean(csubunit(end-10:end));
txt = ['Cc = ', num2str(Cc),' \muM'];
xlabel('Time (s)')
legend(txt)

subplot(row,col,4)

plot(0:time,hydsec)
xlabel('Time (s)')
ylabel('GTPase (uM GTP/min/uM FtsZ)')
ssGTPase = mean(hydsec(end-3:end));
txt = ['Steady State  = ', num2str(ssGTPase),' uM GTP/min/uM FtsZ'];
legend(txt)

endsound