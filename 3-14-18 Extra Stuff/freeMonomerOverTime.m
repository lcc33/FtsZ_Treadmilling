function freeMonomerOverTime(csubunit,time,tstep)
t = 0:tstep:time;
plot(t(2:end),csubunit)
finalC = csubunit(end);
txt = ['Time (s); Final [Monomer] = ',num2str(finalC),' uM'];
xlabel(txt)
ylabel('[Free FtsZ] (uM)')
end