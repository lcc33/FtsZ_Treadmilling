function [numFtsZGDP,numFtsZGTP] = BindingGTPCell(numFtsZGDP,numFtsZGTP,tstep,GtpBindRate)
concConverted = GtpBindRate.*num2conc(numFtsZGDP).*tstep;
numConverted = round(conc2num(concConverted));
numFtsZGTP = numFtsZGTP + numConverted;
numFtsZGDP = numFtsZGDP - numConverted;
end