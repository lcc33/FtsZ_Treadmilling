function [protoSec,concMonomer,hydSec,distMoved] = FtsZ_Treadmilling(concFtsZ,bottomCap,gtpase,time,color,FRET,initialpfs,tstep,kbon,kboff,ktoff,ktlowoff,kRevFav)

totalBottomCap = 600; % only used if MciZ = 1

ktlowon = ktlowoff.*kboff./kbon; % kinetic rate of free monomers adding to top
knr = 10;
knf = 1./kRevFav;

% Initial Protofilaments and FtsZ
totalFtsZ = conc2num(concFtsZ); %total ftsZ subunits in cell
len = 500; % length and width of where pfs are stored
width = 500;

proto = zeros(len,width);
if sum(any(initialpfs)) ~= 0
    [x, y] = size(initialpfs);
    proto(1:x,1:y) = zeros(x,y);
end

% Initiate Values 
totalStep = time./tstep; %total number of time steps
concMonomer = zeros(1,totalStep); % Concentration of free FtsZ monomers
concBottomCap = zeros(1,totalStep); % Concentration of bottom capper
protoSec = uint8(zeros(len,width,time)); % Snapshot of pfs every second
protoSec(:,:,1) = proto;
totalHyd = zeros(1,totalStep+1); 
speed = single(zeros(time-1,1)); % Average speed of pfs each second
pflen = uint16(zeros(time-1,1)); % Average pf length every second
checkprobs = 0; % Used to check that the probability never exceeds 1 for a t step
dimer = zeros(1,totalStep+1); % Total number of dimers each time step
onesec = 1./tstep; % Number of time steps in one second of modelled time
tcount = 1; % Time count to keep up
startLocations = 0; 
distMoved = 0; 

h = waitbar(0,'Calculating...');

% Loop for simulating stuff going off and on for each time frame:
for ii = 1:totalStep % cycling through time

[concMonomer(ii), concBottomCap(ii)] = calcConc(proto,totalFtsZ,totalBottomCap,dimer(ii));
n = lastProtofilament(proto);

[proto,checkprobs,dimer(ii+1)] = Nucleation(proto, tstep,kbon, color,FRET,concMonomer(ii),knf,knr,checkprobs,dimer(ii)); %Allow for nucleation of new pfs
[proto,totalHyd(ii+1)] = gtpaseFunction(proto,gtpase,tstep,totalHyd(ii)); 

    for mm = 1:n % cycling through all of the protofilaments and making sure same action is done to each pf

        probs = rand(1,4);
        
        [proto(:,mm),checkprobs] = bottomOff(proto(:,mm),kboff, tstep,probs(1),checkprobs); %Function for falling off bottom
        [proto(:,mm),checkprobs] = bottomOn(proto(:,mm),kbon,concMonomer(ii),tstep, color,FRET,probs(2),checkprobs); %Function for adding to bottom
        [proto(:,mm),checkprobs] = topOff(proto(:,mm),ktoff,ktlowoff,tstep,probs(3),checkprobs); %Function for falling off top
        [proto(:,mm),checkprobs] = topOn(proto(:,mm),ktlowon,tstep,checkprobs,color,FRET);
        
        [proto(:,mm),checkprobs] = bottomCapOn(proto(:,mm),kbon,concBottomCap(ii),tstep,bottomCap,probs(4),checkprobs); %MciZ adding to bottom
        
    end
    
    waitbar(ii/totalStep)

    if ii>=onesec-5 && mod(ii,onesec) == 1 || mod(ii, onesec) == 0 || mod(ii,onesec) == onesec - 1
        if mod(ii, onesec) == 1 || mod(ii,onesec) == onesec -1
            [startLocations, speed(tcount), tcount,distMoved] = Moved(proto,tcount, ii, onesec,startLocations,distMoved);
        end

        if mod(ii, onesec) == 0
            [proto] = shiftPfLeftUp(proto,ii,onesec);
        end

        if mod(ii,onesec) == onesec - 1 % One count before the second, measure pf length
           pflen(tcount) = ssLenSingle(proto);

        elseif mod(ii,onesec) == 0 % Right on the second
             protoSec(:,:,ii/onesec+1) = proto; %Saves what the pfs look like
        end
    end
    

    
% If you want to see treadmilling in real time, you can uncomment the
% following three lines.
% image(proto(:,1:250)); colormap(protocolormap)
% hold on
% pause(0.01)
%ii.*tstep
    
end

close(h)
% Outputs some data
 % Time to [monomer] reach steady state

hydSec = gtpaseTime(totalHyd,time,tstep,concFtsZ,concMonomer);


end

