clear all; close all;

tstep = 0.01;
time = 30;

% Kinetic Parameters
kbon = 10;
kboff = 1;
ktoff = 8;
ktofflow = 1;

% Things to Test:
KnD = 10000;
indGtpaseRate = [0.1 0.5 1 1.5 2 2.5 5]; % Rate that each individual bound FtsZ can hydrolyze per a second
gtpBindRate = 1;

concTotalFtsZ = 3;

% Experimental Inputs
color = 1;
FRET = 0;


criticalConcentrationHigh = zeros(1,length(KnD));
speed = zeros(1,length(KnD));
pfLength = zeros(1,length(KnD));
hydPerMinuteHigh = zeros(1,length(KnD));
%% Run Treadmilling Model

for ii = 1:length(indGtpaseRate)
    [allPFs, distanceMoved,hydPerSecond,concMonomerPerSecond,numFtsZGDP,numFtsZGTP] = TreadmillCells(time,tstep,concTotalFtsZ,color,FRET,kbon,kboff,ktoff,ktofflow,KnD,indGtpaseRate(ii),gtpBindRate);
    criticalConcentrationHigh(ii) = mean(concMonomerPerSecond(end-3:end));
    speed(ii) = mean(distanceMoved{end});
    pfLength(ii) = mean(length(allPFs{ii}));
    hydPerMinuteHigh(ii) = mean(num2conc(hydPerSecond(end-3:end))./concTotalFtsZ.*60);
end
