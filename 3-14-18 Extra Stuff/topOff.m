function [y,checkprobs] = topOff(singleproto,ktr,kbr,tstep,probs,checkprobs)

y = singleproto;
topoff = ktr.*tstep;

    if topoff > 1
        checkprobs = checkprobs+1;
    end

topoffslow = kbr.*tstep;

q = find(y);  % indexing location of subunits in the pf
if any(y) == 1 && length(q)>1 % must be stuff in pf and more than one monomer

    if  (y(q(2)) == 1 || y(q(2)) == 3) && probs <= topoff % penultimate must be GDP bound, the probability of falling off is higher
        y(q(1)) = 0; % taking off the last subunit off the top      
    elseif  (y(q(2)) == 2 || y(q(2)) == 4) && probs <= topoffslow % penultimate must be GTP bound, the probability of falling off is lower
        y(q(1)) = 0; % taking off the last subunit off the top      
    end
end

if length(find(y)) == 1  %If protofilament is just a single subunit, it goes back into pool           
    y = zeros(length(y),1);
end

end