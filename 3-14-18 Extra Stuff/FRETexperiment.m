%% FRET assembly of protofilaments at steady state.
function [initialpfs,protoSec,concMonomer,hydSec,distMoved,mixHalfTime] = FRETexperiment(concFtsZ,gtpase,time,tstep,kbon,kboff,ktoff,ktlowoff,kRevFav)

bottomCap = 0;
FRET = 0; % If FRET = 1, then FRET reaction. Starts with zero to generate two sets of pfs
initialpfs = 0;

% Create red and green protofilaments

color = 1; % Red pfs
[protoSec] = [protoSec,concMonomer,hydSec,distMoved] = FtsZ_Treadmilling(concFtsZ,bottomCap,gtpase,time,color,FRET,initialpfs,tstep,kbon,kboff,ktoff,ktlowoff,kRevFav);
redpfs = protoSec(:,:,end);

color = 2; % Green pfs
[protoSec] = FtsZ_Treadmilling(concFtsZ,bottomCap,gtpase,time,color,FRET,initialpfs,tstep,kbon,kboff,ktoff,ktlowoff,kRevFav);
greenpfs = protoSec(:,:,end);

% Randomly combine red and green pfs
initialpfs = zeros(size(greenpfs));

len = lastProtofilament(redpfs);
half = round(len./2);
ind1 = randi(len,[1,half]);

initialpfs(:,1:length(ind1)) = redpfs(:,ind1);

len = lastProtofilament(greenpfs);
half = round(len./2);
ind2 = randi(len,[1,half]);

initialpfs(:,length(ind1)+1:length(ind1) +length(ind2)) = greenpfs(:,ind2);

% Treadmill the new combined pfs
FRET = 1; %FRET now becomes 1.
[protoSec,concMonomer,hydSec,distMoved] = FtsZ_Treadmilling(concFtsZ,bottomCap,gtpase,time,color,FRET,initialpfs,tstep,kbon,kboff,ktoff,ktlowoff,kRevFav);

% Determine the percent of each color over time
redProp = zeros(1,time+1);
mixSec = zeros(1,time+1);
measure = 15; % Perform analysis on first 15 columns
t = 0:1:time;
for ii = 1:time+1
    redProp(ii) = length(find(protoSec(:,1:measure,ii) == 2));
    redProp(ii) = redProp(ii)+length(find(protoSec(:,1:measure,ii) == 1));
    total = length(find(protoSec(:,1:measure,ii)));
    mixSec(ii) = redProp(ii)./total;
end

endavg = mean(mixSec(end-3:end));
midvalue = mean([1,endavg]);
[~,ind] = min(abs((midvalue - mixSec)));
mixHalfTime = t(ind); % Time it takes for the pfs to be half way mixed
end
