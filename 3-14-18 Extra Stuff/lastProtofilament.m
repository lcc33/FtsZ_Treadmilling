function last = lastProtofilament(proto)

y = sum(proto,1);
ind = find(y);
if any(ind) == 1
    last = ind(end);
else
    last = 1;
end



end