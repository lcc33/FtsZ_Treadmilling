function protocolor = protocolormap

% % Creates a colormap for subunits in protofilaments; red green blue
% 
% protocolor = zeros(10,3);
% protocolor(1,:) = 0.3; % Nothing is gray
% 
% % When color == 1, red pfs
% protocolor(2,:) = [.8,.36,.36]; % FtsZ-GDP Light red
% protocolor(3,:) = [0.7 0.13 0.13]; % FtsZ-GTP Dark red
% 
% % When color == 2, green pfs
% protocolor(4,:) = [32,178,170]./255; % FtsZ-GDP Light green
% protocolor(5,:) = [46,139,87]./255; % FtsZ-GTP Green
% 
% 
% protocolor(6,:) = [255,215,0]./255; % MciZ is gold


%% Colormap for Grant
protocolor = zeros(3,3);
protocolor(1,:) = 0.3;

protocolor(3,:) = [0.7 0.13 0.13];
protocolor(2,:) = [46,139,87]./255;
end