MciZ = 0; % Boolean, is MciZ in system?
color = 1; % See protocolormap file for key
FRET = 0; % Boolean, is FRET occuring?
initialpfs = 0; % Are there any initial pfs? If yes, make equal to matrix of pfs.

time = 20;
tstep = 0.0001;

gtpase = 6.4;

[protos_sec,csubunit] = FtsZ_Treadmilling(color,time,tstep,MciZ,FRET,initialpfs,gtpase);

freeMonomerOverTime(csubunit,time,tstep)

initialpfs = protos_sec(:,:,end);

time = 80;
tstep = 0.01;

gtpase = 6.4;

[protos_sec,csubunit,speed,pflen,dimer] = FtsZ_Treadmilling(color,time,tstep,MciZ,FRET,initialpfs,gtpase);

figure(2) % Plots histogram of pfs at end
subplot(3,1,1)
ssLength(finalprotos) %Plots the ending length of PFs
subplot(3,1,2)
gtpMakeup(finalprotos,color) %Plots the GTP makeup of ending PFs
subplot(3,1,3)
gdpMakeup(finalprotos,color) %Plots the GTP makeup of ending PFs

% Plots average distance travelled and standard deviation
figure(3)
subplot(3,1,1)
plot(speed)
m = mean(speed(end-5:end));
txt = ['Steady State =  ',num2str(m)];
ylabel('Speed (nm/s)')
legend(txt)

subplot(3,1,2)
plot(pflen)
ylabel('PF Length (subunits)')
m = mean(pflen(end-10:end));
txt =  ['Steady State = ',num2str(m)];
legend(txt)

subplot(3,1,3)
freeMonomerOverTime(csubunit,time,tstep) % plots concentration of free monomer over time)
Cc = mean(csubunit(end-10:end));
txt = ['Cc = ', num2str(Cc),' \muM'];
xlabel('Time (s)')
legend(txt)
