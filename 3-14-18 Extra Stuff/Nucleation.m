function [proto,checkprobs,dimer] = Nucleation(proto, tstep,kbf, color,FRET,csubunit,knf,knr,checkprobs,dimer)
GTP = GTPColorAssign(color,FRET);

rate = knf.*csubunit.^2.*tstep;
rate = rate - knr.*rate.*tstep;
rate = conc2num(rate);

if rate > 1
    rate = round(rate);
elseif rate < 0
    rate = 0;
else
    r = rand;
    if r < rate
        rate = 1;
    else
        rate = 0;
    end
end

dimer = dimer + rate; % Increase the number of dimers by the rate that monomer changed to dimers

nuc = kbf.*csubunit.*tstep;
r = rand(1,dimer);

ind = find(r < nuc);
add = length(ind);
n = lastProtofilament(proto);
proto(21:22,n+1:n+add) = GTP;
proto(23,n+1:n+add) = GTP;

dimer = dimer - add;

end
