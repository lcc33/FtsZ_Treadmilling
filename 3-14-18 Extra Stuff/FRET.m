%% FRET assembly of protofilaments at steady state.

clear all; close all;

MciZ = 0;
time = 20;
tstep = 0.01;
FRET = 1; % If FRET = 1, then FRET reaction.

% Create red and green protofilaments
color = 1; % Red pfs
[protos_sec,redmono, ~, ~, ~, ~ , ~] = FtsZ_Treadmilling(color,time,tstep,MciZ);
redpfs = protos_sec(:,:,end);
redmono = redmono(end);

color = 2; % Green pfs
[protos_sec,greenmono, ~, ~, ~, ~ , ~] = FtsZ_Treadmilling(color,time,tstep,MciZ);
greenpfs = protos_sec(:,:,end);
greenmono = greenmono(end);

% Combine red and green protofilaments
[x, y] = size(redpfs);
combine = zeros(x, y.*2);
lastred = lastProtofilament(redpfs);
lastred = round(lastred./2);
combine(:,1:lastred) = redpfs(:,1:lastred);

lastgreen = lastProtofilament(greenpfs);
lastgreen = round(lastgreen./2);
combine(:,lastred+1:lastgreen+lastred) = greenpfs(:,1:lastgreen);
last = round(0.5*y);
combine = combine(:, 1:last);

totalmono = redmono + greenmono;

imagesc(combine); % shows what the pfs look like

