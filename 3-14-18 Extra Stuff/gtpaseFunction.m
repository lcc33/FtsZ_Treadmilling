
function [protos,totalhyd] = gtpaseFunction(protos,gtpase,tstep,totalhyd)

if sum(any(protos)) > 0 
    ind2 = find(protos == 2);
    ind4 = find(protos == 4);
    ind = [ind2; ind4];
    gtpaserate = length(ind).*tstep.*gtpase./60;
    if gtpaserate < 1
        if rand < gtpaserate
            gtpaserate = 1;
        else 
            gtpaserate = 0;
        end
    else
        gtpaserate = round(gtpaserate);
    end
    
    change = min([gtpaserate, length(ind)]);
    sample = datasample(ind, change,'Replace', false);
    protos(sample) = protos(sample) - 1;
    totalhyd = length(sample);

end
end


