function sslen = ssLenSingle(proto)
y = lastProtofilament(proto);
sslen = zeros(1,y);
for qq = 1:length(sslen)
    m = find(proto(:,qq));
    sslen(qq) = length(m);
end
ind = find(sslen);
sslen = sslen(ind);
sslen = mean(sslen);
end