function [singleproto,checkprobs] = bottomCapOn(singleproto,kbf,concBottomCap,tstep, bottomCap,probs,checkprobs)
if any(singleproto) == 1

bottomCapOnRate = kbf.*concBottomCap.*tstep; 

    if bottomCapOnRate > 1
        checkprobs = checkprobs+1;
    end

q = find(singleproto); % finding location of subunits in the pf matrix
q = q(end)+1; %indexing the location of one above the first subunit in the pf
   if bottomCap == 1 &&  probs <= bottomCapOnRate && singleproto(q-1) ~= 5
        q = find(singleproto>0); % finding location of subunits in the pf matrix
        q = q(end)+1; %indexing the location of one above the first subunit in the pf
        singleproto(q) = 5; % Adding MciZ to bottom
   else
        singleproto = singleproto;
   end
end
end