function gtpase = determineGTPase(gtpase,tstep,totalftsz)

gtpase = gtpase.*totalftsz./60;

gtpase = gtpase.*tstep;
end