function proto = shiftPfLeftUp(proto,ii,onesec)
    if mod(ii,onesec) == 0 % Right on the second
        proto = shiftToTop(proto); %Function to shift pf to top when gets to bottom
        proto = shiftproto(proto); % Shifts pfs to left 
    else
        proto = proto;
    end
end