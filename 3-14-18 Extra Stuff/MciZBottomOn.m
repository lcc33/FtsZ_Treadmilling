function [singleproto,checkprobs] = bottomCapOn(singleproto,kbf,cMciZ,tstep, MciZ,probs,checkprobs)
if any(singleproto) == 1

mciZOnRate = kbf.*cMciZ.*tstep; 

    if mciZOnRate > 1
        checkprobs = checkprobs+1;
    end

q = find(singleproto); % finding location of subunits in the pf matrix
q = q(end)+1; %indexing the location of one above the first subunit in the pf
   if MciZ == 1 &&  probs <= mciZOnRate && singleproto(q-1) ~= 5
        q = find(singleproto>0); % finding location of subunits in the pf matrix
        q = q(end)+1; %indexing the location of one above the first subunit in the pf
        singleproto(q) = 5; % Adding MciZ to bottom
   else
        singleproto = singleproto;
   end
end
end