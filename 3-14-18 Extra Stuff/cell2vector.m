function vector = cell2vector(cell)
vector = zeros(1,length(cell));
for ii = 1: length(cell)
    vector(ii) = str2double(cell2mat(cell(ii)));
end
end