function [lenavg,lenstd] = ssLength(proto)
y = lastProtofilament(proto);
sslen = zeros(1,y);
for qq = 1:length(sslen)
    m = find(proto(:,qq));
    sslen(qq) = length(m);
end
sslen = sslen.*4.3;
edges = 0:10:300;
% histogram(sslen,edges)
ind = find(sslen);
sslen = sslen(ind);
m = histogram(sslen,edges)
set(m, 'FaceColor',[.05 .05 .05])
lenavg = mean(sslen);
lenstd = int32(std(sslen));

xlabel('PF Length (nm)')
ylabel('Frequency')
end