function [y,checkprobs] = topOn(singleproto,kbf,tstep,checkprobs,color,FRET)

GTP = GTPColorAssign(color,FRET);

y = singleproto;
topon = kbf.*tstep;

    if topon > 1
        checkprobs = checkprobs+1;
    end
r = rand;
q = find(y);  % indexing location of subunits in the pf
if any(y) == 1 && length(q)>1 % must be stuff in pf and more than one monomer

    if  (y(q(2)) == 2 || y(q(2)) == 4) && r <= topon % penultimate must be GDP bound, the probability of falling off is higher
        y(q(1)-1) = GTP; % taking off the last subunit off the top         
    end
end

end