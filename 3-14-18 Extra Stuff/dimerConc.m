function cdimer = dimerConc(csubunit, Kn)

cdimer = Kn.*csubunit.^2;

end