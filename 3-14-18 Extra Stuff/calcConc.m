function [csubunit,cMciZ] = calcConc(proto,totalftsz, totalMciZ,dimer)

    subunits =  totalftsz - length(find(proto>0 &proto<5)); % calculate new number of free subunits
    csubunit = num2conc(subunits) - num2conc(dimer); % calculate concentration of free subunits and subtract dimer

    freeMciZ = totalMciZ - length(find(proto == 5));
    cMciZ = num2conc(freeMciZ);
end