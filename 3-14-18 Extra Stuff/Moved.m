function [startLocations, disttravel, tcount,distmoved] = Moved(proto,tcount, ii, onesec, startLocations,distmoved)

if mod(ii,onesec) == 1 % Right after the second, determine start locations
    startLocations = bottomMonomer(proto);
    disttravel = 0;
elseif mod(ii,onesec) == onesec - 1 % Right before the second, determine end locations and dist travelled
    endLocations = bottomMonomer(proto);
    ind1 = find(startLocations ~= 1000);
    ind2 = find(endLocations ~=1000);
    ind = intersect(ind1,ind2);
    distmoved = (endLocations(ind)-startLocations(ind)).*4.3;
    disttravel = mean(distmoved);
    startLocations = 0;
    tcount = tcount + 1;
end
end