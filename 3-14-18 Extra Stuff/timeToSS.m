function sstime = timeToSS(csubunit,time,tstep)

t = 0:tstep:time;
t = t(2:end);
mm = 1;

twosec = 2./tstep;

span = .1./tstep;
twentysec = 15./tstep;
for ii = twosec:span:twentysec
    tchunk = t(ii:ii+span-1);
    chunk = csubunit(ii:ii+span-1);
    p = polyfit(tchunk,chunk,1);
    slope(mm) = p(1);
    mm = mm + 1;
end

ind = find(abs(slope) < 0.1);
sstime = ind(1).*.1+2;

end