function [activesub] = gtpExchange(allsubunits,tstep,time,ii)

t = 0:tstep:time;
inactivesub = allsubunits.*exp(-t.*2);
activesub = allsubunits - inactivesub(ii);

end