function [y,checkprobs] = bottomOff(singleproto,kbr,tstep,probs,checkprobs)

y = singleproto;

bottomoff = kbr.*tstep;

if probs <= bottomoff && any(singleproto) == 1
   q = find(singleproto>0); % Finding location of subunits in the protofilament matrix
   y(q(end)) = 0; %Taking off one subunit from end of protofilament
end
    if bottomoff > 1
        checkprobs = checkprobs+1;
    end

if length(find(y)) == 1  %If protofilament is just a single subunit, it goes back into pool           
    y = zeros(length(y),1);
end

end