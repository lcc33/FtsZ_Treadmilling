function proto = shiftToTop(proto)
 lastpf = lastProtofilament(proto);
        for index = 1:lastpf
            p = proto(:,index); %saves individual pf
            if sum(p(end-50:end)) > 0 % if any of the last 50 subunits are greater than zero
                [ind, ~, value] = find(p);
                p = zeros(size(p));
                p(25:25+length(ind)-1) = value;
            end
            proto(:,index) = p;
        end
end
