function [GDP, GTP] = colorAssigner(color)

if color == 1
    GDP = 1;
    GTP = 2;
elseif color == 2
    GDP = 3;
    GTP = 4;
end

end
