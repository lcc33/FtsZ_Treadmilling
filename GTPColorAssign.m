function GTP = GTPColorAssign(color, FRET)
[~, GTP] = colorAssigner(color);
r = rand;
if FRET == 1 && r < 0.5
    GTP = 4;
elseif FRET == 1 && r >= 0.5
    GTP = 2; 
end

end