function locations = Spots(proto)
mm = size(proto,2);
locations = zeros(1,mm);
for ii = 1:mm
    locations(ii) = Spot(proto(:,ii));
end
end
