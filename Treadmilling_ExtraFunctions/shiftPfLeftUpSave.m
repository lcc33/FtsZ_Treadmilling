function proto = shiftPfLeftUp(proto)
    if mod(ii,onesec) == 0 % Right on the second
        protos_sec(:,:,ii/onesec+1) = proto; %Saves what the pfs look like
        proto = shiftToTop(proto); %Function to shift pf to top when gets to bottom
        proto = shiftproto(proto); % Shifts pfs to left 
    else
        proto = proto;
end