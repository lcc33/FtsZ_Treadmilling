[m,n, o] = size(allprotos);
pull = 1:10:o;
%suballprotos = zeros(m,n,length(pull));
suballprotos = allprotos(:,:,pull);

for ii = 1:length(pull)
    image(suballprotos(:,:,ii).*30)
    pause(0.1)
end