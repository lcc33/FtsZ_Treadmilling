function [proto] = treadmilling(totalp,totalftsz, tstep, time, kbf,kbr,ktr, gtpase);

totalstep = time./tstep;
proto = zeros(200, totalp, totalstep);
proto(50:100,1,1) = 1;

for ii = 2:totalstep % cycling through time
    
    subunits =  totalftsz - length(find(proto(:,:,ii)>0)); % calculate new number of free subunits
    csubunit = subunits.*10.^6/(2.*10.^-15.*6.022.*10.^23); % calculate concentration of free subunits
   
    bottomoff = kbr.*tstep; %Rate of subunits falling off top of proto
    bottomon = kbf.*csubunit.*tstep; % rate of subunits adding to top of proto. changes with concentration of free subunits
    topoff = ktr.*tstep; % rate of subunits falling off bottom
    gtpaserate = gtpase.*tstep;
    proto(:,:,ii) = proto(:,:,ii-1);
    
    for mm = 1:totalp % cycling through all of the protofilaments and making sure same action is done to each pf
        r = rand(1,3); % create a random matrix of three numbers to base adding and subtracting rate on
        if any(proto(:,mm,ii)) == 1 & r(1) <= bottomoff % subunits falling off top; any function makes sure that you won't remove subunits from a nonexistant protofilament
            q = find(proto(:,mm,ii)>0); % Finding location of subunits in the protofilament matrix
            proto(q(end),mm) = 0; % setting the top nonzero to zero, so it falls off the top
        end
        if any(proto(:,mm,ii)) == 1 & r(2) <= bottomon %subunits adding to top
            q = find(proto(:,mm,ii)>0); % finding location of subunits in the pf matrix
            q = q(end)+1; %indexing the location of one above the first subunit in the pf
            proto(q,mm,ii) = 2; % Adding one GTP bound FtsZ to the top
        end
        if any(proto(:,mm,ii)) == 1 & r(3) <= topoff; %subunits falling off bottom
            q = find(proto(:,mm,ii)>0); % indexing location of subunits in the pf
            if length(q)>1
            if proto(q(2),mm,ii) == 1 % now only will remove the bottom subunit if the second from the bottom is GDP bound
                proto(q(1),mm,ii) = 0; % taking off the last subunit off the bottom
            end
            end
            %proto(q(end),mm) = 0;
        end

        r = rand(length(proto(:,mm,ii))); %make a new random number matrix that is same length as pf
        for jj = 1: length(r) % cycle through the length of the pf
            if r(jj) <= gtpaserate & proto(jj,mm,ii) ==2 % if the random number is less than the rate calculated and it is GTPbound...
                proto(jj,mm,ii) = 1; % it now becomes GDP bound
            end
        end
    
if proto(end,mm,ii)> 0 % if the pf has reached the top of the matrix
    q = find(proto(:,mm,ii)>0); % determines index of all subunits in pf
    num = proto(q,mm,ii); %saves the makeup of the pf
    proto(:,mm,ii) =0; %sets the entire matrix to zero
    proto(1:length(num),mm,ii) = num; %puts the saved makeup of pf at the top
end
    end
end
   




