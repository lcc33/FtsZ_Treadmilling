clear all;

time = 2;
tstep = 0.01;
kbr = 2;
kbf = 100;
ktr = 8;
totalftsz = 6000;

totaltime = time./tstep;

proto = zeros(200,1,totaltime); %initiate protofilament
proto(51:97,1:end,1) = 1; 
proto(98:100,1:end,1) = 2;
mm = 1;

for ii = 2:totaltime
    subunits =  totalftsz - length(find(proto(:,:,ii-1)>0)); % calculate new number of free subunits
    csubunit = subunits.*10.^6/(2.*10.^-15.*6.022.*10.^23); % calculate concentration of free subunits
   
    bottomoff = kbr.*tstep; %Rate of subunits falling off top of proto
    bottomon = kbf.*csubunit.*tstep; % rate of subunits adding to top of proto. changes with concentration of free subunits
    topoff = ktr.*tstep; % rate of subunits falling off bottom
%    gtpaserate = gtpase.*tstep;
    r = rand(1,4);
    
    proto(:,:,ii) = proto(:,:,ii-1);
    
%     if any(proto(:,mm,ii)) == 1 & r(1) <= bottomoff % subunits falling off bottom; any function makes sure that you won't remove subunits from a nonexistant protofilament
%         q = find(proto(:,mm,ii)>0); % Finding location of subunits in the protofilament matrix
%         proto(q(end),mm,ii) = 0; % setting the top nonzero to zero, so it falls off the bottom
%     end
    
    if any(proto(:,mm,ii)) == 1 && r(2) <= bottomon
        q = find(proto(:,mm,ii)>0); % finding location of subunits in the pf matrix
        q = q(end)+1; %indexing the location of one above the first subunit in the pf
        proto(q,mm,ii) = 2; % Adding one GTP bound FtsZ to the bottom
    end
    
    if any(proto(:,mm,ii)) == 1 & r(3) <= topoff; %subunits falling off top
        q = find(proto(:,mm,ii)>0); % indexing location of subunits in the pf
        if length(q)>1
            if proto(q(2)) == 1 % now only will remove the bottom subunit if the second from the bottom is GDP bound
                proto(q(1)) = 0; % taking off the last subunit off the bottom
            end
        end
        if length(q) == 1 & proto(q) == 1
            proto(:,mm,ii) = 0;
        end

    end
    
end