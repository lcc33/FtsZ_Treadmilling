function [mid,averagemid] = midpoint(allprotos)
[subunits,protos,time] = size(allprotos);
mid = zeros(protos,time);
    for ii = 1:time
        for mm = 1:protos
            q = find(allprotos(:,mm,ii));
            halflen = fix(length(q)/2);
            mid(mm,ii) = q(halflen);
        end
    end
averagemid = mean(mid,1);
end