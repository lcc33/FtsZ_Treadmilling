clear all; close all; %makes it so previous code and calculations won't affect current stuff. 

%% Simulation of pfs falling off and on and hydrolysis

totalp = 100; % the total number of pfs is 100
proto = zeros(200,totalp); % sets of pf matrix. Just zeros
totalftsz = 6750; %total ftsZ subunits in cell
totalMciZ = 600;

tstep = 0.01; %the time step is set to 0.01 seconds
time = 2000; % this simulates 20 seconds
totalstep = time./tstep; %total number of time steps

% Make the proto have a cap of 3 GDP. Rest are GTP bound. All are 20
% subunits long.
proto(51:97,1:end) = 1; %the protofilaments have a three GTP bound cap on top
proto(98:100,1:end) = 2; %the pfs are 50 subunits long. The 1 represents GDP bound

% Set rate constants. GTPase probably needs to change
kbf = 10;
kbr = 2;
ktr = 8;
gtpase = 0.5;

% Simulate subunits falling off top of proto

checkoff = zeros(1,totalstep);
checkon = zeros(1,totalstep);
checkoffb = zeros(1,totalstep);

cc = zeros(1,totalstep);
BoundMciZ = zeros(1,totalstep);
currenttime = 0;

for ii = 1:totalstep % cycling through time
    
    subunits =  totalftsz - length(find(proto>0&proto <3)); % calculate new number of free subunits
    csubunit = subunits.*10.^6/(2.*10.^-15.*6.022.*10.^23); % calculate concentration of free subunits
   
    freeMciZ = totalMciZ - length(find(proto == 3));
    
    bottomoff = kbr.*tstep; %Rate of subunits falling off top of proto
    bottomon = kbf.*csubunit.*tstep; % rate of subunits adding to top of proto. changes with concentration of free subunits
    topoff = ktr.*tstep; % rate of subunits falling off bottom
    gtpaserate = gtpase.*tstep;
    
    propMciZ = freeMciZ./subunits;
    
 %% Subunits Adding and Subtracting
    for mm = 1:totalp % cycling through all of the protofilaments and making sure same action is done to each pf
    r = rand(1,4); % create a random matrix of three numbers to base adding and subtracting rate on

% Subunits Falling off Bottom
    if any(proto(:,mm)) == 1 & r(3) <= topoff; %subunits falling off bottom
        q = find(proto(:,mm)>0); % indexing location of subunits in the pf
        if length(q)>1
        if proto(q(2),mm) == 1 % now only will remove the bottom subunit if the second from the bottom is GDP bound
            proto(q(1),mm) = 0; % taking off the last subunit off the bottom
        end
        end
    
% Subunits Adding to Bottom
    if any(proto(:,mm)) == 1 & r(2) <= bottomon
        q = find(proto(:,mm)>0); % finding location of subunits in the pf matrix
        q = q(end)+1; %indexing the location of one above the first subunit in the pf
        if proto(q-1,mm) ~= 3
            if r(4) >= propMciZ
                proto(q,mm) = 2; % Adding one GTP bound FtsZ to the top
            else 
                proto(q,mm) = 3;
            end
        end
    end
    
% Subunits falling off Top
    if any(proto(:,mm)) == 1 & r(3) <= topoff; %subunits falling off bottom
        q = find(proto(:,mm)>0); % indexing location of subunits in the pf
        if length(q)>=2
            if proto(q(2),mm) == 1 % now only will remove the bottom subunit if the second from the bottom is GDP bound
                proto(q(1),mm) = 0; % taking off the last subunit off the bottom
            end
        else 
            proto(:,mm) = 0;
        end
        %proto(q(end),mm) = 0;
    end
   
    
%% GTPase Rate 
    r = rand(length(proto(:,mm))); %make a new random number matrix that is same length as pf
    for jj = 1: length(r) % cycle through the length of the pf
        if r(jj) <= gtpaserate & proto(jj,mm) ==2 % if the random number is less than the rate calculated and it is GTPbound...
            proto(jj,mm) = 1; % it now becomes GDP bound
        end
    end
%% Move Protofilament to Top
if proto(end,mm)> 0 % if the pf has reached the top of the matrix
    q = find(proto(:,mm)>0); % determines index of all subunits in pf
    num = proto(q,mm); %saves the makeup of the pf
    proto(:,mm) =0; %sets the entire matrix to zero
    proto(1:length(num),mm) = num; %puts the saved makeup of pf at the top
end

    end
    
%% Plot Treadmilling in Real Time
cc(ii) = csubunit;
BoundMciZ(ii) = length(find(proto == 3));
    if rem(ii,10) == 0
        
        subplot(6,3,[1:12])
        image(proto.*20) %outputs an image of pf treadmilling
        xlabel('Protofilament Number')

    
        subplot(6,3,[16:18])

        barh(currenttime)
        xlim([0,time])
        xlabel('Time (s)')
        pause(0.1)
    end
 currenttime = currenttime+tstep;
 %gives a small time between new plots so you can actually see it
end
%% Output data
% Determine length of each pf
len = zeros(1,totalp);
for qq = 1:totalp
    clear m
    m = find(proto(:,qq)>0);
    len(qq) = length(m);
end

figure
subplot(5,1,1)
histogram(len)
xlabel('Length of Protofilament');
ylabel('Frequency');
average_length = mean(len)

% Determine GTP makeup of each pf
gtp = zeros(1,totalp);
for qq = 1:totalp
    clear m
    m = find(proto(:,qq)==2);
    gtp(qq) = length(m);
end

subplot(5,1,2)
histogram(gtp)
xlabel('Number of GTP Bound Subunits per Protofilament')
ylabel('Frequency')
average_GTP = mean(gtp)

% Determine GDP makeup of each pf
gdp = zeros(1,totalp);
for qq = 1:totalp
    clear m
    m = find(proto(:,qq)==1);
    gdp(qq) = length(m);
end

subplot(5,1,3)
histogram(gdp)
xlabel('Number of GDP Bound Subunits per Protofilament')
ylabel('Frequency')
average_GDP = mean(gdp)

t = 0:tstep:time;
subplot(5,1,4)
plot(t(2:end),cc)
xlabel('Time (s)')
ylabel('Monomer Concentration (uM)')

subplot(5,1,5)
plot(t(2:end),BoundMciZ)
xlabel('Time (s)')
ylabel('Bound MciZ Molecules')