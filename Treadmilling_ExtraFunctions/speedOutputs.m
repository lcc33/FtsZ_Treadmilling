function speedOutputs(disttravel,time,tstep,totalproto)

disttravel = sum(disttravel,2);
disttravel = disttravel.*4.3; %each monomer is 5 nm long.
avg = disttravel./totalproto';
t = 0:tstep*2:time;

whole = find(mod(t,1) == 0);
whole = whole - 1;
speed = zeros(1,length(whole)-1);
for ii = 2:length(whole)
    speed(ii-1) = sum(avg(whole(ii-1)+1:whole(ii)));
end
wholetime = 1:1:time;

totaldist = sum(avg);
distpersec = totaldist./time;


%subplot(2,1,1)
% plot(t(2:end),avg)
% txt = ['Average Distance Traveled per time step (nm)'];
% ylabel(txt)
% txt = ['Time (s); Total Distance Per Second = ', num2str(distpersec), ' nm/s'];
% xlabel(txt);

%subplot(2,1,2)
bar(wholetime,speed)
txt = ['Time (s); Total Distance Per Second = ', num2str(distpersec), ' nm/s'];
xlabel(txt);
ylabel('Speed (nm/s)');

end