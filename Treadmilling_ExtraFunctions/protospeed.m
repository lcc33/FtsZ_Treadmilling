function protospeed(mid,tstep,time)
[x,y] = size(mid);
averagemid = mean(mid,1);
speed = zeros(x,y-2);
speed = diff(mid,1,2);
t = 0:tstep:time;

avgspeedtime = mean(speed,1);
stdspeedtime = std(speed,0,1);

subplot(2,1,1)
plot(t(2:end-1),avgspeedtime)
ylabel('Avg PF Speed')
subplot(2,1,2)
plot(t(2:end-1),stdspeedtime)
xlabel('Time (s)')
ylabel('Std Dev')
end