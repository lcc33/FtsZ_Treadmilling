function middlepoint = Spot(singlepf)
if any(singlepf) == 1
    pf = find(singlepf);
    halflength = fix(length(pf)/2);
    middlepoint = pf(halflength);
else
    middlepoint = 0;
end
end