function spot = Location(singleProto)
    idx=find(singleProto);
    spot = singleProto(idx(round(numel(idx)/2)));
end

