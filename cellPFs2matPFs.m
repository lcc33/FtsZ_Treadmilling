function matrixPF = cellPFs2matPFs(cellPF,allPFLocation)
matrixPF = zeros(length(allPFLocation));
for ii = 1:length(cellPF)
    indPF = cell2mat(cellPF(ii));
    len = length(indPF);
    matrixPF(allPFLocation(ii)+51:allPFLocation(ii)+len+50,ii) = indPF;
end
end