function [allPFs, distanceMoved,hydPerSecond,concMonomerPerSecond] = TreadmillCells(time,tstep,concTotalFtsZ,concCap,color,FRET,kbon,kboff,ktoff,ktofflow,kton,ktonlow,KnD,kcapon,kcapoff,indGtpaseRate);

% Initiate Parameters
expectedPFs = 500;
totalTimeSteps = time./tstep;
stepsPerSecond = 1./tstep;
concMonomer = concTotalFtsZ;

dimer = 0;
currentTime = 0;
hydCount = 0;

% Data to Collect
distanceMoved = {};
hydPerSecond = zeros(1,time - 1);
allPFs = {[1; 1; 1; 2; 2;]};
allPFLocation = zeros(1,expectedPFs);
trackHydCount = zeros(1,totalTimeSteps);

h = waitbar(0,'Calculating...');

for ii = 1:totalTimeSteps
    
    [allPFs,dimer] = NucleationCell(allPFs, tstep,kbon, color,FRET,concMonomer,KnD,dimer);
    
    totalBoundFtsZ = length(cell2mat(allPFs'));
    concBoundFtsZ = num2conc(totalBoundFtsZ);
    concMonomer = concTotalFtsZ - concBoundFtsZ;

    
    
    for mm = 1:length(allPFs) % Events that happen to individual PFs each time step
        allPFs{mm} = BottomOnCell(allPFs{mm},kbon,kcapon,tstep,concMonomer,concCap,color,FRET);
        allPFs{mm} = BottomOffCell(allPFs{mm},kboff,kcapoff,tstep);
        [allPFs{mm},allPFLocation(mm)] = TopOnCell(allPFs{mm},kton,ktonlow,concMonomer,tstep,allPFLocation(mm),color,FRET);
        [allPFs{mm},allPFLocation(mm)] = TopOffCell(allPFs{mm},ktoff,ktofflow,tstep,allPFLocation(mm));
        
        [allPFs{mm},hydCount] = GTPaseCell(allPFs{mm},indGtpaseRate,tstep,hydCount);

    end
     
    waitbar(ii/totalTimeSteps)
trackHydCount(ii) = hydCount;
    %% Collect Data at Certain Time Points
    % Show a snapshot of PFs every second from beginning
%     if mod(ii,stepsPerSecond.*tstep.*10) == 0
%         pfSnapShot = cellPFs2matPFs(allPFs,allPFLocation);
%         imagesc(pfSnapShot)
%         pause(tstep)
%     end
    
    % Data to collect at beginning of each second
    if mod(ii,stepsPerSecond) == 1
        locationSnapShot = allPFLocation; % Location of each PF
        beginningLength = length(allPFs);
        currentTime = currentTime + 1; % Current time in loop
        hydStart = hydCount; % Current number already hydrolyzed
    end
    
    % Data to collect at end of each second
    if mod(ii,stepsPerSecond) == 0 && ii > stepsPerSecond-2
        endLength = length(allPFs); % Counts total PFs present
        pfsToMeasure = min([beginningLength endLength]);
        distanceMoved{currentTime} = allPFLocation(1:pfsToMeasure) - locationSnapShot(1:pfsToMeasure); % Distance moved over second
        hydPerSecond(currentTime) = hydCount-hydStart; % Additional FtsZ hydrolyzed in second
        concMonomerPerSecond(currentTime) = concMonomer;
    end
    
    % Get rid of empty pfs
    if mod(ii,stepsPerSecond) == 0 && ii > stepsPerSecond-2
        isZero = cellfun(@(x)isequal(x,0),allPFs);
        ind = find(isZero);
        for qq = 1:length(ind)
            allPFs{ind(qq)} = {};
        end
        allPFs = allPFs(~cellfun('isempty',allPFs));
    end
    

end
concMonomerPerSecond = [concTotalFtsZ concMonomerPerSecond];
close(h)
end