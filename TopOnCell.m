function [singlePF,singlePFLocation] = TopOnCell(singlePF,kton,ktonlow,concMonomer,tstep,singlePFLocation,color,FRET)
GTP = GTPColorAssign(color,FRET);
if  length(singlePF) > 2
    if (singlePF(2) == 1) || (singlePF(2) == 3)
        if rand < ktonlow.*tstep.*concMonomer
            singlePF = [GTP; singlePF];
            singlePFLocation = singlePFLocation - 1;
        end
    elseif (singlePF(2) == 2) || (singlePF(2) == 4)
        if rand < kton.*tstep.*concMonomer
            singlePF = [GTP; singlePF];
            singlePFLocation = singlePFLocation - 1;
        end
    end
else
    singlePF = 0;
end
end