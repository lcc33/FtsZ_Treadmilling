function [singlePF,hydCount] = GTPaseCell(singlePF,indGtpaseRate,tstep,hydCount)
    if length(singlePF)>2 % If it is a nucleated pf
        for ii = 1:length(singlePF) % Scroll through PF and perform following action on each subunit in pf
            if rand<indGtpaseRate.*tstep % A certain probability of the time
                if singlePF(ii) == 2 || singlePF(ii) == 4 % If it is GTP bound
                    singlePF(ii) = singlePF(ii) - 1; % It becomes GDP bound
                    hydCount = hydCount + 1; % Keep track of how much as been hydrolyzed for data analysis
                end
            end
        end
    else
        singlePF = 0; % If it isn't a nucleated pf, get rid of it. 
    end
end