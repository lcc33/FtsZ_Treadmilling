function singlePF = BottomOffCell(singlePF,kboff,kcapoff,tstep)
if singlePF(end)<5
    if rand < kboff.*tstep % Event: bottom subunit falling off when it is FtsZ
        singlePF = singlePF(1:end-1);
    end
elseif singlePF(end) == 5 % Event: bottom subunit falling off when it is a bottom capper
    if rand < kcapoff.*tstep
        singlePF = singlePF(1:end-1);
    end
end

    if length(singlePF) < 3 % Get rid of pfs that are only dimers
       singlePF = 0;
    end
    
end