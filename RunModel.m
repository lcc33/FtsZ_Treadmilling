classdef RunModel < handle

    properties
        Figure
        TitlePanel
        Title
        Panel1
        Panel2
        Panel3
        Panel4
        Panel5
        Panel6
        
        TreadmillDiagram
        TreadmillLabel
        TreadmillDiagramLow
        CapDiagram
        
        GTPaseDiagram
        GTPaseInput
        GTPaseLabel
        
        TimeInput
        TimeLabel
        
        ConcFtsZInput
        ConcFtsZLabel
        
        KbOnInput
        KbOnInputLabel
        
        KbOffInput
        KbOffInputLabel
        
        KtOffInput
        KtOffInputLabel
        
        KtOnInput
        KtOnInputLabel
        
        KtLowOffInput
        KtLowOffInputLabel
        
        KtLowOnInput
        KtLowOnInputLabel
        
        KCapOffInput
        KCapOffInputLabel
        
        KCapOnInput
        KCapOnInputLabel
        
        ConcCapInput
        ConcCapInputLabel
        
        NucleationDiagram
        KnDInput
        KnDLabel
        
        BottomCap
        PreformedPFs
        PreformedPFsLabel
        GoButton
        CloseButton
    end
    
    methods
        function app = RunModel
            app.Figure = figure('MenuBar','none','NumberTitle','off','Name','Treadmilling Experiment','Position',[200 50 900 700],'CloseRequestFcn',@closeApp);
            app.TitlePanel = uipanel('Parent',app.Figure,'Position',[0.3 0.92 0.4 0.06]); 
            app.Title = uicontrol('Parent',app.TitlePanel,'Style','Text','String','FtsZ Treadmilling Experiment','fontsize',20,'Position',[25 10 300 24]);
            
            cd ModelDiagrams
            
            %% WT diagram
            app.Panel1 = uipanel('Parent',app.Figure,'BackgroundColor','w','Position', [.025 .05 .3 0.85]);
            app.TreadmillDiagram = axes('Parent',app.Panel1,'Position',[0 0 1 1]); % Creates axes to plot on
            highDiagram = imread('Slide1.jpg');
            app.TreadmillDiagram = imshow(highDiagram);
            app.TreadmillLabel = uicontrol('Parent', app.Panel1,'BackgroundColor','w','Style','Text','String','Top subunit is in low affinity state.','FontSize',14,'Position',[2 450 115 50]);
            
            app.KbOnInput = uicontrol('Parent',app.Panel1,'Style','edit','String','10','Units','normalized','Position',[0.01 0.15 0.1 0.04]);
            app.KbOnInputLabel = uibutton('Parent',app.Panel1,'String','\muM^-^1s^-^1','Interpreter','tex','Units','normalized','Position',[0.11 0.15 0.2 0.04]);
            
            app.KbOffInput = uicontrol('Parent',app.Panel1,'Style','edit','String','1','Units','normalized','Position',[0.71 0.15 0.1 0.04]);
            app.KbOffInputLabel = uibutton('Parent',app.Panel1,'String','s^-^1','Interpreter','tex','Units','normalized','Position',[0.81 0.15 0.2 0.04]);
            
            app.KtOffInput = uicontrol('Parent',app.Panel1,'Style','edit','String','8','Units','normalized','Position',[0.71 0.82 0.1 0.04]);
            app.KtOffInputLabel = uibutton('Parent',app.Panel1,'String','s^-^1','Interpreter','tex','Units','normalized','Position',[0.81 0.82 0.2 0.04]);
            
            app.KtOnInput = uicontrol('Parent',app.Panel1,'Style','edit','String','1','Units','normalized','Position',[0.01 0.82 0.1 0.04]);
            app.KtOnInputLabel = uibutton('Parent',app.Panel1,'String','\muM^-^1s^-^1','Interpreter','tex','Units','normalized','Position',[0.11 0.82 0.2 0.04]);
            
            %% Low GTPase Diagram
            app.Panel2 = uipanel('Parent',app.Figure,'BackgroundColor','w','Position', [.35 .41 .3 0.49]);
            app.TreadmillDiagramLow = axes('Parent',app.Panel2,'Position',[0 0 1 1]); % Creates axes to plot on
            LowDiagram = imread('Slide2.jpg');
            [x, ~, ~] = size(LowDiagram);
            LowDiagram = LowDiagram(1:int64(x*.58),:,:);
            app.TreadmillDiagramLow = imshow(LowDiagram);
            
            app.KtLowOffInput = uicontrol('Parent',app.Panel2,'Style','edit','String','0.1','Units','normalized','Position',[0.71 0.69 0.1 0.07]);
            app.KtLowOffInputLabel = uibutton('Parent',app.Panel2,'String','s^-^1','Interpreter','tex','Units','normalized','Position',[0.81 0.69 0.2 0.07]);
            
            app.KtLowOnInput = uicontrol('Parent',app.Panel2,'Style','edit','String','1','Units','normalized','Position',[0.01 0.69 0.1 0.07]);
            app.KtLowOnInputLabel = uibutton('Parent',app.Panel2,'String','\muM^-^1s^-^1','Interpreter','tex','Units','normalized','Position',[0.11 0.69 0.2 0.07]);
            
            %% Bottom Capper
            app.Panel3 = uipanel('Parent',app.Figure,'BackgroundColor','w','Position',[0.67 .4 .3 0.5]);
            app.CapDiagram = axes('Parent',app.Panel3,'Position',[0 0 1 1]); % Creates axes to plot on
            capdiagram = imread('Slide3.jpg');
            [x, ~, ~] = size(capdiagram);
            capdiagram = capdiagram(int64(x*.4):end,:,:);
            app.CapDiagram = imshow(capdiagram);
            
            app.KCapOnInput = uicontrol('Parent',app.Panel3,'Style','edit','String','10','Units','normalized','Position',[0.01 0.25 0.1 0.07]);
            app.KCapOnInputLabel = uibutton('Parent',app.Panel3,'String','\muM^-^1s^-^1','Interpreter','tex','Units','normalized','Position',[0.11 0.25 0.2 0.07]);
            
            app.KCapOffInput = uicontrol('Parent',app.Panel3,'Style','edit','String','1','Units','normalized','Position',[0.71 0.25 0.1 0.07]);
            app.KCapOffInputLabel = uibutton('Parent',app.Panel3,'String','s^-^1','Interpreter','tex','Units','normalized','Position',[0.81 0.25 0.2 0.07]);
            
            app.ConcCapInputLabel = uibutton('Parent',app.Panel3,'String','[Bottom Cap] (\muM):','Interpreter','tex','Units','normalized','Position',[0.05 0.01 0.4 0.07]);
            app.ConcCapInput = uicontrol('Parent',app.Panel3,'Style','edit','String','0','Units','normalized','Position',[0.45 0.01 0.1 0.07]);
            
            %% Nucleation
            app.Panel4 = uipanel('Parent',app.Figure,'BackgroundColor','w','Position',[0.35 .22 .3 0.17]);
            app.NucleationDiagram = axes('Parent',app.Panel4,'Position',[0 0 1 1]); % Creates axes to plot on
            nucleation = imread('DimerReaction.jpg');
            app.NucleationDiagram = imshow(nucleation);
            
            app.KnDInput = uicontrol('Parent',app.Panel4,'Style','edit','String','100','Units','normalized','Position',[0.42 0.2 0.2 0.2]);
            app.KnDLabel = uibutton('Parent',app.Panel4,'String','\muM (K_d)','Interpreter','tex','Units','normalized','Position',[0.62 0.2 0.2 0.2]);
            %% GTPase
            app.Panel5 = uipanel('Parent',app.Figure,'BackgroundColor','w','Position',[0.35 .05 .3 0.15]);
            app.GTPaseDiagram = axes('Parent',app.Panel5,'Position',[0 0 1 1]);
            gtpasediagram = imread('GTPaseDiagram.jpg');
            [x, y, ~] = size(gtpasediagram);
            gtpasediagram = gtpasediagram(int64(x.*0.07):int64(x.*0.28),1:int64(0.8.*y),:);
            app.GTPaseDiagram = imshow(gtpasediagram);
            
            app.GTPaseInput = uicontrol('Parent',app.Panel5,'Style', 'edit','String','0.5','Units','normalized','Position', [0.4 0.2 0.1 0.2]);
            app.GTPaseLabel = uibutton('Parent',app.Panel5,'String','s^-^1','Interpreter','tex','Units','normalized','Position',[0.5 0.2 0.1 0.2]);
            %% Other Inputs
            app.Panel6 = uipanel('Parent',app.Figure,'Position',[0.67 .05 .3 0.32]);
            app.PreformedPFs = uicontrol('Parent',app.Panel6,'Style','checkbox','Position', [10 170 20 20]);
            app.PreformedPFsLabel = uicontrol('Parent',app.Panel6,'Style','Text','String','Run Experiment on Preformed PFs','FontSize',14,'Position',[30 172 225 20]);
            app.TimeInput = uicontrol('Parent',app.Panel6,'Style','edit','String','10','Position', [90 130 50 20]);
            app.TimeLabel = uicontrol('Parent',app.Panel6,'Style','Text','String','Time (s)','FontSize',14,'Position',[10 130 80 20]);
            app.ConcFtsZInput = uicontrol('Parent',app.Panel6,'Style','edit','String','6','Position', [90 100 50 20]);
            app.ConcFtsZLabel = uicontrol('Parent',app.Panel6,'Style','Text','String','[FtsZ] (uM)','FontSize',14,'Position',[10 100 80 20]);
           
            app.GoButton = uicontrol('Parent',app.Panel6,'Style','pushbutton','String','Run Experiment','FontSize',16,'Position',[10 50 120 20],'BackgroundColor',[152,251,152]/256,'Callback',@startExperiment);
            app.CloseButton = uicontrol('Parent',app.Panel6,'Style','pushbutton','String','Quit Program','FontSize',16,'Position', [10 10 120 20],'BackgroundColor',[256,92,92]/256,'Callback',@closeExperiment);
            
            cd ../
            function closeExperiment(source,event)
                delete(app.Figure)
            end
           
            function startExperiment(source,event)
                
                indGtpaseRate = str2double(app.GTPaseInput.String);
                kbon = str2double(app.KbOnInput.String);
                kboff = str2double(app.KbOffInput.String);
                ktoff = str2double(app.KtOffInput.String);
                kton = str2double(app.KtOnInput.String);
                ktonlow = str2double(app.KtLowOffInput.String);
                KnD = str2double(app.KnDInput.String);
                ktofflow = str2double(app.KtLowOffInput.String);
                kcapon = str2double(app.KCapOnInput.String);
                kcapoff = str2double(app.KCapOffInput.String);
                concCap = str2double(app.ConcCapInput.String);
                concTotalFtsZ = str2double(app.ConcFtsZInput.String);
                time = str2double(app.TimeInput.String);
                
                color = 1;
                FRET = 0;
                initialpfs = 0;
                tstep = 0.01;
                bottomCap = 0;
                
                closereq
                
                [allPFs, distanceMoved,hydPerSecond,concMonomerPerSecond] = TreadmillCells(time,tstep,concTotalFtsZ,concCap,color,FRET,kbon,kboff,ktoff,ktofflow,kton,ktonlow,KnD,kcapon,kcapoff,indGtpaseRate);
                
                
               %% Analyze Collected Data
figure
len = 2;
wid = 2;

% SS PF Speed
subplot(len,wid,1)
hist(distanceMoved{end})
xlabel('SS Distance Moved (subunits)')
ylabel('Frequency')

% SS PF Length
pfLength = zeros(1,length(allPFs));
for ii = 1:length(allPFs)
    pfLength(ii) = length(allPFs{ii});
end
subplot(len,wid,2)
hist(pfLength)
xlabel('SS PF Length (subunits)')
ylabel('Frequency')

% Hydrolysis
hydPerMinute = num2conc(hydPerSecond)./concTotalFtsZ.*60;
subplot(len,wid,3)
plot(1:time,hydPerMinute)
xlabel('Time (sec)')
ylabel('GTPase')

% Concentration of Monomer
subplot(len,wid,4)
plot(0:time,concMonomerPerSecond)
xlabel('Time (sec)')
ylabel('[Monomer]')
            end
           
        end
    end
end